5# MIT License

# Copyright (c) 2020 Tamado Ramot Sitohang

# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:

# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.

# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

import re
import pandas as pd

from unidecode import unidecode
from nltk import sent_tokenize
from wordcloud import WordCloud

# nltk.download('punkt')


class WordCloudCreator:
    def stopword_creator(self, filename):
        try:
            return [line.replace("\n", "") for line in open(filename, "r").readlines()]
        except FileNotFoundError:
            raise FileNotFoundError("Error, file tidak ditemukan!")

    def dataframes_lists(self, csv_file):
        try:
            return [pd.read_csv(csv_file)]
        except FileNotFoundError:
            raise FileNotFoundError("Error, file tidak ditemukan!")
        except ValueError:
            raise Exception("File CSV tidak memiliki kolom caption atau username!")

    def tokenizer(self, dataframes_list):
        return [
            [
                sentence
                for caption in df["caption"]
                for sentence in sent_tokenize(str(caption))
            ]
            for df in dataframes_list
        ]

    def create_word_list(self, captions_frame):
        allcaps = []
        for captions in captions_frame:
            captions = ",".join(captions)
            captions = re.sub(r"@\w+[._]\w+ *|@\w+", "", captions)
            captions = re.sub(r"#\w+", "", captions)
            captions = re.sub(
                r"[^\x00-\x7F\x80-\xFF\u0100-\u017F\u0180-\u024F\u1E00-\u1EFF]",
                "",
                captions,
            )
            captions = re.sub(r"[,\.\"!?\[\]\:\|\/\(\)\_\“\”\‘\’\n]", " ", captions)
            captions = re.sub(r"[\-\'\=]", "", captions)
            captions = re.sub(r"[0-9]", "", captions)
            captions = captions.lower()
            captions = [word for word in captions.split()]
            captions = [unidecode(caption) for caption in captions]
            allcaps.append(captions)
        return " ".join([word for caption in allcaps for word in caption])

    def create_wordcloud(
        self, max_words, stopwords, username, word_list, width, height, text_only=False
    ):
        wordcloud = WordCloud(
            stopwords=stopwords,
            background_color="white",
            max_words=max_words,
            contour_width=3,
            collocations=False,
            contour_color="steelblue",
            width=width,
            height=height,
        )
        wordcloud_array = wordcloud.process_text(word_list)
        wordcloud_dataframe = (
            pd.DataFrame.from_dict(wordcloud_array, orient="index", columns=["jumlah"])
            .reset_index()
            .rename(columns={"index": "kata"})
            .sort_values("jumlah", ascending=False)
        )
        if not text_only:
            wordcloud.generate(word_list)
            wordcloud_image = wordcloud.to_image()
            wordcloud_image.save(username + "-" + str(max_words) + ".jpg")
        else:
            wordcloud_dataframe.to_csv(username + "-daftar-kata" + ".csv", index=False)


if __name__ == "__main__":
    wordcloud_creator_instance = WordCloudCreator()
    filename = input("Nama file stopwords? ")
    stopwords = wordcloud_creator_instance.stopword_creator(filename)
    csv_file = input("Nama file csv? ")
    dataframe = wordcloud_creator_instance.dataframes_lists(csv_file)
    word_tokens = wordcloud_creator_instance.tokenizer(dataframe)
    word_list = wordcloud_creator_instance.create_word_list(word_tokens)
    username = csv_file.rstrip(".csv")
    print("processing...")
    for i in [10, 25, 50, 100]:
        wordcloud_creator_instance.create_wordcloud(
            i, stopwords, username, word_list, 1600, 1600
        )
    wordcloud_creator_instance.create_wordcloud(
        0, stopwords, username, word_list, 1, 1, text_only=True
    )
    print("Finished!")
